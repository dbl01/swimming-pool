#include "SwimmingPool.h"
#include "Tests.h"

typedef const void test();

static test* tests[] =
{
	testCreateObject,
	testPrintObject,
	testShowCapacity,
	testAmountOfWaterFillCompletely,
	testAmountOfTimeFillCompletely,
	testFillToCapacityAndDisplay,
	testDisplayTimeToDrainCompletely,
	testDrainHalfAndDisplay,
	testAmountOfTimeToFillHalf,
	testAdd3HWaterAndDisplay,
	testFillOverCapacity,
	static_cast<test*>(0)
};

int main()
{
	size_t at = 0;
	while (tests[at])
	{
		std::cout << "TEST " << at + 1 << ":\n";
		tests[at++]();
		std::cout << "\n";
	}
	std::cout << std::endl << at << " tests passed./n";
	system("pause");
	return 0;
}
