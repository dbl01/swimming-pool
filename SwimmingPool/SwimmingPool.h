#pragma once
#include<iostream>

const double gallons = 7.48;

struct SwimmingPool
{
	double length, width, depth, amountOfWaterInPool, flowRateIn, flowRateOut;

	SwimmingPool(double initialLength = 0.0, double initialWidth = 0.0, double initialDepth = 0.0, double initialAmountOfWaterInPool = 0.0, double initialFlowRateIn = 0.0, double initialFlowRateOut = 0.0);

	const void print() const;
	const double getAmountOfWaterInPool() const;
	const double getPoolTotalCapacity() const;
	const double getTimeToFillPool() const;
	const double getTimeToDrainPool() const;
	const double getAmountNeededToFill() const;
	void addWater(const double timeInMinutes, const double flowRate);
	void drainWater(const double timeInMinutes, const double flowRate);
};