#include "SwimmingPool.h"

SwimmingPool::SwimmingPool(double initialLength, double initialWidth, double initialDepth, double initialAmountOfWaterInPool, double initialFlowRateIn, double initialFlowRateOut)
	:length(initialLength), width(initialWidth), depth(initialDepth), amountOfWaterInPool(initialAmountOfWaterInPool), flowRateIn(initialFlowRateIn), flowRateOut(initialFlowRateOut)
{

}

const void SwimmingPool::print() const
{
	std::cout << "Pool data: \n";
	std::cout << "Length: " << length << ".\n";
	std::cout << "Width: " << width << ".\n";
	std::cout << "Depth: " << depth << ".\n";
	std::cout << "Amount of water in pool: " << amountOfWaterInPool << ".\n";
	std::cout << "Fill rate: " << flowRateIn << ".\n";
	std::cout << "Drain rate: " << flowRateOut << ".\n";
}

const double SwimmingPool::getAmountOfWaterInPool() const
{
	return amountOfWaterInPool;
}

const double SwimmingPool::getPoolTotalCapacity() const
{
	return length * width * depth * gallons;
}

const double SwimmingPool::getTimeToFillPool() const
{
	if (flowRateIn)
	{
		return (getPoolTotalCapacity() - amountOfWaterInPool) / flowRateIn;
	}
	return 0.0;
}

const double SwimmingPool::getTimeToDrainPool() const
{
	if (flowRateOut)
	{
		return amountOfWaterInPool / flowRateOut;
	}
	return 0.0;
}

const double SwimmingPool::getAmountNeededToFill() const
{
	return getPoolTotalCapacity() - getAmountOfWaterInPool();
}

void SwimmingPool::addWater(const double timeInMinutes, const double flowRate)
{
	if (timeInMinutes < 0 || flowRate < 0)
	{
		throw "The selected time or flow is a negative value!\n";
	}
	else
	{
		amountOfWaterInPool = amountOfWaterInPool + flowRate * timeInMinutes;

		double capacity = getPoolTotalCapacity();
		if (amountOfWaterInPool > capacity)
		{
			amountOfWaterInPool = capacity;
			throw "Warning! Pool filled over the maximum capacity!\n";
		}
	}
}

void SwimmingPool::drainWater(const double timeInMinutes, const double flowRate)
{
	if (flowRate <= 0)
	{
		throw "Can't drain water because the flow rate is smaller or equal to 0 gallons/min!\n";
	}
	if (amountOfWaterInPool > 0)
	{
		amountOfWaterInPool -= flowRate * timeInMinutes;
		if (amountOfWaterInPool < 0)
		{
			amountOfWaterInPool = 0;
		}
	}
	else
	{
		throw "It's impossible to drain water because it's no water in the swimming pool!\n";
	}
}
