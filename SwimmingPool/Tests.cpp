#include "Tests.h"
#include "SwimmingPool.h"

const void testCreateObject() 
{
	SwimmingPool swimmingPool(30, 15, 10, 0, 20.0, 19.5);
}

const void testPrintObject()
{
	SwimmingPool swimmingPool(30, 15, 10, 0, 20.0, 19.5);
	swimmingPool.print();
}

const void testShowCapacity()
{
	SwimmingPool swimmingPool(30, 15, 10, 0, 20.0, 19.5);
	std::cout << "The capacity of the pool is: " << swimmingPool.getPoolTotalCapacity() << ".\n";
	assert(33660 == swimmingPool.getPoolTotalCapacity());
}

const void testAmountOfWaterFillCompletely()
{
	SwimmingPool swimmingPool(30, 15, 10, 0, 20.0, 19.5);
	std::cout << "The amount of water needed to fill completely the pool is: " << swimmingPool.getAmountNeededToFill() << ".\n";
	assert(33660 == swimmingPool.getAmountNeededToFill());
}

const void testAmountOfTimeFillCompletely()
{
	SwimmingPool swimmingPool(30, 15, 10, 0, 20.0, 19.5);
	double totalMinutes = swimmingPool.getPoolTotalCapacity() / swimmingPool.flowRateIn;
	int hoursToFill = totalMinutes / 60;
	double minutesToFill = totalMinutes - hoursToFill * 60;
	std::cout << "Time needed to fill the pool: " << hoursToFill << " hours " << minutesToFill << " minutes. \n ";

}

const void testFillToCapacityAndDisplay()
{
	SwimmingPool swimmingPool(30, 15, 10, 0, 20.0, 19.5);

	while (swimmingPool.amountOfWaterInPool <= swimmingPool.getPoolTotalCapacity())
	{
		swimmingPool.amountOfWaterInPool = swimmingPool.amountOfWaterInPool + swimmingPool.flowRateIn;
	}

	if (swimmingPool.amountOfWaterInPool > swimmingPool.getPoolTotalCapacity())
	{
		double overflow = swimmingPool.amountOfWaterInPool - swimmingPool.getPoolTotalCapacity();
		swimmingPool.amountOfWaterInPool = swimmingPool.amountOfWaterInPool - overflow;
	}

	std::cout << "The amount of water after filling the pool is: " << swimmingPool.amountOfWaterInPool << ".\n";
}

const void testDisplayTimeToDrainCompletely()
{
	SwimmingPool swimmingPool(30, 15, 10, 0, 20.0, 19.5);
	double totalMinutes = swimmingPool.getPoolTotalCapacity() / swimmingPool.flowRateOut;
	int hoursToDrain = totalMinutes / 60;
	int minutesToDrain = totalMinutes - hoursToDrain * 60;
	std::cout << "Time needed to drain the pool: " << hoursToDrain << " hours " << minutesToDrain << " minutes. \n ";
}

const void testDrainHalfAndDisplay()
{
	SwimmingPool swimmingPool(30, 15, 10, 0, 20.0, 19.5);
	swimmingPool.amountOfWaterInPool = swimmingPool.getPoolTotalCapacity();
	swimmingPool.drainWater(1, swimmingPool.getPoolTotalCapacity() / 2);
	std::cout << "Amount of water left after draining half the pool: " << swimmingPool.getAmountOfWaterInPool() << " gallons. \n";
}

const void testAmountOfTimeToFillHalf()
{
	SwimmingPool swimmingPool(30, 15, 10, 0, 20.0, 19.5);
	swimmingPool.amountOfWaterInPool = swimmingPool.getPoolTotalCapacity();
	swimmingPool.drainWater(1, swimmingPool.getPoolTotalCapacity() / 2);
	double time = swimmingPool.getTimeToFillPool();
	std::cout << "Time needed to fill the pool if it is half full: " << round(time / 60) << " hours " << (int)time % 60 << " minutes. \n";
}

const void testAdd3HWaterAndDisplay()
{
	SwimmingPool swimmingPool(30, 15, 10, 0, 20.0, 19.5);
	swimmingPool.amountOfWaterInPool = swimmingPool.getPoolTotalCapacity();
	swimmingPool.drainWater(1, swimmingPool.getPoolTotalCapacity() / 2);
	swimmingPool.addWater(180, swimmingPool.flowRateIn);
	std::cout << "Amount of water after adding water an additional 3 hours: " << swimmingPool.getAmountOfWaterInPool() << " gallons.\n";
}

const void testFillOverCapacity()
{
	SwimmingPool swimmingPool(30, 15, 10, 0, 20.0, 19.5);
	swimmingPool.addWater(120, 1000);
}