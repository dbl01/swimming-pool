#pragma once
#include <cassert> //If expression evaluates to 0 (false), then the expression, sourcecode filename, and line number are sent 
                  //to the standard error, and then abort() function is called.

const void testCreateObject();
const void testPrintObject();
const void testShowCapacity();
const void testAmountOfWaterFillCompletely();
const void testAmountOfTimeFillCompletely();
const void testFillToCapacityAndDisplay();
const void testDisplayTimeToDrainCompletely();
const void testDrainHalfAndDisplay();
const void testAmountOfTimeToFillHalf();
const void testAdd3HWaterAndDisplay();
const void testFillOverCapacity();